package com.wissentechnology.saturn.common.annotations;


import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation to identify invoker method for RequestService class
 * 
 * @author Vivek Shende
 *
 */
@Retention(RUNTIME)
@Target({METHOD})
public @interface ServiceInit {

}
