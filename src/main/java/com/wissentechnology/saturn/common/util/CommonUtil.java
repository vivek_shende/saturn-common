package com.wissentechnology.saturn.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class
 * @author
 *
 */
public class CommonUtil {

	private static final Logger LOGGER = Logger.getLogger(CommonUtil.class.getName());

	private CommonUtil() {
	}
	
	public static String urlEncode(String str) {
		try {
	         return URLEncoder.encode(str, "utf8").replaceAll("\\+", "%20");
	    } catch (UnsupportedEncodingException e) {
	       return str;
	    }
	}
	
	public static Date parseDate(String dateString,String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = dateFormat.parse(dateString);
		} catch (ParseException e) {
			LOGGER.log(Level.SEVERE, "Invalid date format");
		}
		
		return date;
	}
	
	public static Date parseDate(String dateString) {
		return parseDate(dateString,"yyyy-MM-dd");
	}
}
