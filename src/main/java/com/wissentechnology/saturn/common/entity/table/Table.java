package com.wissentechnology.saturn.common.entity.table;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.wissentechnology.saturn.common.entity.QueryResult;

/**
 * Represents a Table output of a query.
 * This class is immutable. Use TableBuilder to construct this object.
 * @author sameer
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "Table")
public final class Table implements QueryResult, Serializable {

	private static final Logger LOGGER = Logger.getLogger(Table.class.getName());
	private static final long serialVersionUID = -862746726108145159L;

	@XmlElement(name = "header")
	Header header;

	@XmlElement(name = "rows")
	Row[] rows;

	public Table(){}

	public Table(Header header, Row[] rows) {
		this.header = header;
		this.rows = rows;

		LOGGER.log(Level.INFO,"New Table created. header= {0}   numRows= {1}",
				new Object[]{header.toString(),rows.length});
	}

	public Row[] getRows() {
		return rows;
	}

	public Header getHeader() {
		return header;
	}
}
