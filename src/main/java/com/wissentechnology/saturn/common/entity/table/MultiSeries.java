package com.wissentechnology.saturn.common.entity.table;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "TimeSeries")
public final class MultiSeries implements Serializable {
	private static final long serialVersionUID = -5644546110663248363L;
	
	@XmlElement(name = "labels")
	String[] labels;
	
	@XmlElement(name = "series")
	Integer[] seriesValues;

	public MultiSeries() {

	}

	public MultiSeries(String[] xAxisLabels, Integer[] seriesValues) {
		super();
		this.labels = xAxisLabels;
		this.seriesValues = seriesValues;
	}

	public String[] getxAxisLabels() {
		return labels;
	}

	public Integer[] getSeriesValues() {
		return seriesValues;
	}
}
