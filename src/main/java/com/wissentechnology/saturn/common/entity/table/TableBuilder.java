package com.wissentechnology.saturn.common.entity.table;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder class to construct a Table object (as Table object is immutable.
 * @author sameer
 *
 */
public class TableBuilder {
	private List<Column> columns = new ArrayList<>();
	private List<Row> rows = new ArrayList<>();

	public TableBuilder addColumn(String columnName) {
		columns.add(new Column(columnName));
		return this;
	}

	public TableBuilder addRow(Row row) {
		rows.add(row);
		return this;
	}

	public Table toTable() {
		return new Table(new Header(columns.toArray(new Column[0])), rows.toArray(new Row[0]));
	}
}