package com.wissentechnology.saturn.common.entity;

/**
 * Represents the result of a Query service.
 *
 * @author sameer
 *
 */
public interface QueryResult extends Result {

}