package com.wissentechnology.saturn.common.entity;

/**
 * Represents the result of a Update service.
 * @author
 *
 */

public interface UpdateResult extends Result {

}
