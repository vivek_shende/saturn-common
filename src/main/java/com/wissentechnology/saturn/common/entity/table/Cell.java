package com.wissentechnology.saturn.common.entity.table;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "Cell")
public final class Cell implements Serializable {

	private static final long serialVersionUID = -2611157096668601182L;

	private static final String NULL_VALUE = "-";

	@XmlElement(name = "value")
	public Object value;

	public Cell() {

	}

	public Cell(Object value) {
		super();
		if (value == null) {
			value = NULL_VALUE;
		}
		this.value = value;
	}

	public Object getValue() {
		return value;
	}
}
