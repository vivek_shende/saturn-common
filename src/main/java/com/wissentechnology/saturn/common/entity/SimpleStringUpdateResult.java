package com.wissentechnology.saturn.common.entity;

import java.io.Serializable;

public class SimpleStringUpdateResult implements UpdateResult, Serializable {

	private static final long serialVersionUID = -2019282166446045137L;

	private String result;

	public SimpleStringUpdateResult(String result) {
		super();
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
