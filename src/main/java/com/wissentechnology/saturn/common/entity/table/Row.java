package com.wissentechnology.saturn.common.entity.table;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "Row")
public final class Row implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8502178881577384711L;
	

	@XmlElement(name = "cells")
	public Cell[] cells;

	public Row() {

	}

	public Row(Cell... cells) {
		super();
		this.cells = cells;
	}

	public Row(Object... values) {
		super();
		Cell[] cells = new Cell[values.length];
		int num = 0;
		for (Object value : values) {
			cells[num++] = new Cell(value);
		}

		this.cells = cells;
	}

	public Cell[] getCells() {
		return cells;
	}
}
