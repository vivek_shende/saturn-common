package com.wissentechnology.saturn.common.entity;


public class UpdateResultImpl implements UpdateResult {

	private Boolean isSuccessful;

	public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessfull) {
		this.isSuccessful = isSuccessfull;
	}

	@Override
	public String toString() {
		return "UpdateResultImpl [isSuccessfull=" + isSuccessful + "]";
	}
}
