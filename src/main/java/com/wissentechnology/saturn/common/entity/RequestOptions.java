package com.wissentechnology.saturn.common.entity;

import java.io.Serializable;


/**
 * Any options passed to the request service to indicate how to execute and return result.
 * @author sameer
 *
 */
public class RequestOptions implements Serializable {

	private static final long serialVersionUID = 1L;

	public RequestOptions() {
		// TODO Auto-generated constructor stub
	}

}