package com.wissentechnology.saturn.common.entity;

import java.io.Serializable;

public class SimpleStringQueryResult implements QueryResult, Serializable {

	private static final long serialVersionUID = 7171197618424266832L;
	
	private String result;

	public SimpleStringQueryResult(String result) {
		super();
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
