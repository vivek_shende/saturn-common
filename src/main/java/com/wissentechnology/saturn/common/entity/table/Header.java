package com.wissentechnology.saturn.common.entity.table;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "TableHeader")
public class Header {

	@XmlElement(name = "attributes")
	Map<String, String> attributes = new HashMap<>();

	@XmlElement(name = "columns")
	Column[] columns;

	Header() {

	}

	public Header(Column[] columns, Map<String, String> attributes) {
		this(columns);
		this.attributes.putAll(attributes);
	}

	public Header(Column[] columns) {
		this.columns = columns;
	}

	public Column[] getColumns() {
		return columns;
	}

	public String getAttribute(String attributeName) {
		return attributes.get(attributeName);
	}

	@Override
	public String toString() {
		return "Header [" + (attributes != null ? "attributes=" + attributes + ", " : "")
				+ (columns != null ? "columns=" + Arrays.toString(columns) : "") + "]";
	}
}
